/**
 * Read .logfind line per line
 * Apply glob patterns
 */

#ifndef __logfiles_h__
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <glob.h>
#include "dbg.h"
#include "trim.h"

#ifndef LOGFIND
#define LOGFIND ".logfind"
#endif

#undef NDEBUG

#define NLOGFILES 2048
#define LOGFIND_LINE_BUFFER 1024

char **logfiles;

FILE *open_logfind()
    /* Return a stream associated to the logfind file
     * The functions first seeks to open the file in the
     * local directory and then in the home folder.
     */
{
    char *file = realpath(__FILE__, NULL);
    char *curdir = dirname(file);

    strcat(curdir, "/");
    log_info("Searching for '%s'", curdir);
    FILE *input = fopen(strcat(curdir, LOGFIND), "r");
    if (input != NULL) {
        free(file);
        return input;
    }

    log_info("'%s' not found.", curdir);
    free(file);
    char *homedir = getenv("HOME");
    check(homedir != NULL, "$HOME not set.");

    strcat(homedir, "/");
    input = fopen(strcat(homedir, LOGFIND), "r");
    log_info("Searching for '%s'", homedir);
    check(input != NULL, "Need '%s' to parse the logfile names.", homedir);

    return input;

error:
    exit(-1);

}

void *read_logfind()
{
    FILE *input = open_logfind();
    logfiles = calloc(NLOGFILES, sizeof(char *));

    ssize_t nchars;
    size_t n = LOGFIND_LINE_BUFFER;

    // prepare for globbing
    char *pattern = calloc(LOGFIND_LINE_BUFFER, sizeof(char));
    glob_t pglob = { };
    int glob_rc;
    while (1) {
        nchars = getline(&pattern, &n, input);
        if (nchars == -1) {
            break;
        }
        rtrim(pattern, NULL);
        debug("%s: %ld", pattern, nchars);
        // glob
        glob_rc = glob(
            pattern,
            GLOB_APPEND | GLOB_TILDE | GLOB_PERIOD,
            NULL,
            &pglob);
        if (glob_rc == GLOB_NOMATCH) continue;
        check(glob_rc == 0,
              "Failed to glob with return value %d. [Aborted: %d; "
              "No space: %d]",
              glob_rc,
              GLOB_ABORTED,
              GLOB_NOSPACE);
        log_info("Matched %ld paths so far.", pglob.gl_pathc);
    }

    int i = 0;
    for (i = 0; i < pglob.gl_pathc; i++) {
        logfiles[i] = calloc(LOGFIND_LINE_BUFFER, sizeof(char));
        strncpy(logfiles[i], pglob.gl_pathv[i], LOGFIND_LINE_BUFFER);
        debug("Will process %s [%p]", logfiles[i], &logfiles[i]);
    }
    int rc = fclose(input);
    check(rc == 0, "File not closed successfully.");

error:
    globfree(&pglob);
    free(pattern);
    return NULL;
}

void gc_logfind()
{
    int i = 0;
    while (logfiles[i]) {
        debug("==> Freeing memory for '%s' at %p", logfiles[i], logfiles[i]);
        free(logfiles[i++]);
    }
    free(logfiles);
}


#endif
