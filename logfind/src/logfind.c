#include <regex.h>
#include <stdio.h>
#include <pthread.h>
#include "./logfiles.h"

#define MAX_TOKEN_LENGTH 256
#define MAX_TOKENS 512
#define NTHREADS 12

typedef struct {
    const char *filename;
    char **tokens;
} thread_data;

extern char **logfiles;

char *reg_token(const char *token)
/**
 * Return the token prepended and appended with '\b' regex
 * special character that matches the empty string at the
 * start and end of a word
 *
 * The return value is a pointer to the modified token
 * that is malloc(3)ated and should be free(3)d.
 */
{
    char *btoken = malloc(strlen(token) + 2*strlen("\\b") + 1);
    return strcat(strcat(strcpy(btoken, "\\b"), token), "\\b");
}

regex_t *regor(char **tokens)
{
    regex_t *preg = malloc(sizeof(regex_t));
    int i = 0;
    char *pattern = malloc(MAX_TOKEN_LENGTH);
    char **btokens = calloc(MAX_TOKENS, sizeof(char *));
    *btokens = reg_token(*tokens);
    strcpy(pattern, *btokens);
    while (tokens[++i]) {
        btokens[i] = reg_token(tokens[i]);
        strcat(strcat(pattern, "|"), btokens[i]);
    }
    debug("Compiling pattern '%s'", pattern);

    int rc = regcomp(
        preg, pattern,
        REG_EXTENDED | REG_ICASE | REG_NEWLINE
        );
    free(pattern);
    i = 0;
    while(btokens[i]) {
        free(btokens[i++]);
    }
    free(btokens);
    check(rc == 0, "Could not compile regex for %s", tokens[i]);

    return preg;

error:
    regfree(preg);
    free(preg);
    perror(NULL);
    exit(1);
}

void *match_or(void *data)
{
    thread_data *input_data;
    input_data = (thread_data *) data;
    const char *filename = input_data->filename;
    char **tokens = input_data->tokens;
    FILE *file = fopen(filename, "r");
    // Construct the regular expression
    regex_t *preg = regor(tokens);

    // Find the maches
    int rc;
    ssize_t nchars;
    size_t n = 0;
    char *lineptr = NULL;

    while (1) {
      nchars = getline(&lineptr, &n, file);
      if (nchars == -1) break;

      rc = regexec(preg, lineptr, 0, NULL, 0);
      if (rc == 0) {
          printf("==> [%s]: %s", filename, lineptr);
      } else if (rc == REG_NOMATCH) {
          continue;
      } else {
          log_err("This is problematic!.");
          goto error;
      }
    }
    free(lineptr);

error:
    fclose(file);
    regfree(preg);
    free(preg);
    return NULL;
}

void *match_all(void *data)
{
    thread_data *input_data;
    input_data = (thread_data *) data;
    const char *filename = input_data->filename;
    char **tokens = input_data->tokens;
    FILE *file = fopen(filename, "r");

    // Construct the regular expressions
    regex_t **preg = calloc(MAX_TOKENS, sizeof(regex_t *));
    regex_t preg_bs[MAX_TOKENS];
    int i = 0, rc;
    char *pattern;

    int ntokens = 0;
    while (tokens[i]) {
        pattern = reg_token(tokens[i]);
        preg[i] = &preg_bs[i];
        debug("(%d): Compiling regex for '%s' at %p", i, pattern, preg[i]);
        rc = regcomp(
            preg[i], pattern,
            REG_EXTENDED | REG_ICASE | REG_NEWLINE | REG_NOSUB
            );
        free(pattern);
        check(rc == 0, "Could not compile regex for %s", tokens[i]);
        ntokens = ++i;
    }
    debug("Number of tokens: %d", ntokens);

    // Find the maches
    ssize_t nchars;
    size_t n = 0;
    char *lineptr = NULL;
    int line_match_count;

    while (1) {
      nchars = getline(&lineptr, &n, file);
      if (nchars == -1) break;

      line_match_count = 0;
      for (i = 0; preg[i]; i++) {
          rc = regexec(preg[i], lineptr, 0, NULL, 0);
          if (rc == 0) {
              line_match_count++;
          } else if (rc == REG_NOMATCH) {
              continue;
          } else {
              log_err("This is problematic!.");
              goto error;
          }
      }

      if (line_match_count)
          debug("Found %d matches on %s", line_match_count, lineptr);
      if (line_match_count == ntokens) {
          printf("==> [%s]: %s", filename, lineptr);
      }
    }
    free(lineptr);

error:
    fclose(file);
    i = 0;
    while (preg[i]) {
        regfree(preg[i++]);
    }
    free(preg);
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t threads[NTHREADS];
    thread_data td[NTHREADS];

    read_logfind();
    int i = 0;
    while (logfiles[i]) {
        td[i].filename = logfiles[i];
        if (strcmp(argv[1], "-o") == 0) {
            td[i].tokens = argv + 2;
            pthread_create(&threads[i], NULL, (void *)match_or, (void *) &td[i]);
            log_info("Started thread %ld", threads[i]);
        } else {
            td[i].tokens = argv + 1;
            pthread_create(&threads[i], NULL, (void *)match_all, (void *) &td[i]);
            log_info("Started thread %ld", threads[i]);
        }
        i++;
    }
    for (i = 0; logfiles[i]; i++) {
        pthread_join(threads[i], NULL);
    }
    gc_logfind();
    return 0;
}
