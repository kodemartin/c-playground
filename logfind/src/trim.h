#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "dbg.h"

char *ltrim(char *string, const char *chars)
{
    size_t totrim;
    if (chars == NULL) {
        chars = " \t\n\v\r\f";
    }

    totrim = strspn(string, chars);
    if (totrim > 0) {
        size_t len = strlen(string);
        if (totrim == len) {
            string[0] = '\0';
        } else {
            memmove(string, string + totrim, len - totrim + 1);
        }
    }
    return string;
}

char *left_trim(char *string, const char *chars)
    /***
     * Does not trim the string IN-PLACE;
     */
{
    if (chars == NULL) {
        chars = " \t\n\v\r\f";
    }

    size_t len = strlen(string);

    int i = 0;
    while (i < len && strchr(chars, string[i]) != NULL) {
        i++;
    }

    if (i == len) {
        *string = '\0';
    } else {
        string += i;
    }

    return string;
}

char *rtrim(char *string, const char *chars)
{
    int i;
    if (chars == NULL) {
        chars = " \t\n\v\r\f";
    }

    i = strlen(string) - 1;
    while (i >= 0 && strchr(chars, string[i]) != NULL ) {
            string[i] = '\0';
            i--;
    }
    return string;
}

char *trim(char *string, const char *chars) {
    string = rtrim(string, chars);
    string = left_trim(string, chars);
    log_info("Trimmed to '%s'", string);
    return string;
}
