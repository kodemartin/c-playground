# C Playground

A collection of small projects realized while learning C.

## Contents

* [logfind](logfind/): A tool for matching exact tokens line-per-line in a set
  of specified files.
