# `logfind` tool

My take on the `logfind` exercise from the [Learn C the hard way][lcthw] set of
exercises.

## Build

```
$ make logfind
```

## Features

* Scan files specified in a `.logfind` file. File should be placed either in
  `HOME` or in the source-code directory.
* Use `glob` to expand wildcards in `.logfind`.
* Parse each specified file **line-per-line** to match a series of tokens
  passed through the command line. This uses `regex` to match **exact**
  tokens according to the `\b<token>\b` pattern.
* Use multiple threads to process the files with `pthread`.

## Usage

```
$ logfind [-o] <token1> ... <tokenn>

By default matches a line only if all tokens are found in a line.

-o: OR condition. I.e. match any of the tokens.
```

## Requirements

* `gcc`
* The debug macros provides in the course (See [`dbg.h`][dbg]).

[lcthw]: https://github.com/zedshaw/learn-c-the-hard-way-lectures
[dbg]: https://github.com/zedshaw/learn-c-the-hard-way-lectures/blob/master/ex19/dbg.h
